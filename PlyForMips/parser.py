import ply.yacc as yacc
import lexer # Import lexer information
tokens = lexer.tokens # Need token list


class Segment():
    def __init__(self, label, seg_type, index, stmt_list, at_branch):
        self.label = label
        self.seg_type = seg_type
        self.index = index
        self.stmt_list = stmt_list
        self.at_branch = at_branch

class Program():
    def __init__(self, segment_list):
        self.segment_list = segment_list

curSegment = Segment('','',0,[], False)
curProgram = Program([])

def p_program(p):
    '''program : stmt_list'''

def p_stmt_list(p):
    '''stmt_list : stmt
                 | stmt_list stmt'''
    p[0] = []
    if p[1] is not None:
        p[0].append(p[1])
    if len(p) > 2:
        if p[2] is not None:
            p[1].append(p[2])
        p[0] = p[1]


def p_stmt(p):
    '''stmt : EOL
             | instruction EOL
             | segment EOL
             | labelInstruction EOL
             | controlInstruction EOL'''
    if len(p) != 2:
        p[0] = p[1]
        curSegment.stmt_list.append(p[1])
    if curSegment.at_branch == True:
        tempSegment = Segment(curSegment.label, curSegment.seg_type, curSegment.index, curSegment.stmt_list.copy(),
                              curSegment.at_branch)
        curProgram.segment_list.append(tempSegment)
        curSegment.label = ''
        curSegment.stmt_list = []
        curSegment.index += 1
        curSegment.at_branch = False

def p_labelInstruction(p):
    '''labelInstruction : LABEL
                        | LABEL DIRECTIVE STRING
                        | LABEL DIRECTIVE NUMBER'''
    if(len(p) == 4):
        p[0] = []
        dict = {'declare label:': p[1]}
        p[0].insert(0, dict)
        p[0].insert(1, p[2])
        p[0].insert(2, p[3])
    else:
        p[0] = []
        dict = {'label:': p[1]}
        p[0].insert(0, dict)
        if len(curSegment.stmt_list) != 1:
            tempSegment = Segment(curSegment.label, curSegment.seg_type, curSegment.index, curSegment.stmt_list.copy(),
                                  curSegment.at_branch)
            curProgram.segment_list.append(tempSegment)
        else:
            curSegment.index -= 1
        curSegment.label = p[1]
        curSegment.stmt_list = []
        curSegment.index += 1

def p_controlInstruction(p):
    '''controlInstruction : JUMPINSTRUCTION STRING
                          | BRANCHINSTRUCTION REGISTER STRING
                          | BRANCHINSTRUCTION REGISTER REGISTER STRING'''
    p[0] = []
    for i in range(len(p)):
        if i != 0:
            p[0].append(p[i])
    curSegment.at_branch = True

def p_segment(p):
    '''segment : SEGMENT'''
    p[0] = p[1]
    new_index = curSegment.index + 1
    if new_index != 1:
        tempSegment = Segment(curSegment.label, curSegment.seg_type, curSegment.index, curSegment.stmt_list.copy(),
                              curSegment.at_branch)
        curProgram.segment_list.append(tempSegment)
        curSegment.index = new_index
        curSegment.seg_type = p[1]
        curSegment.label = ''
        curSegment.stmt_list = []
        # print("segment: {0}".format(p[1]))
    else:
        curSegment.index += 1

def p_instruction(p):
    '''instruction : SYSCALL
                    | RINSTRUCTION REGISTER REGISTER REGISTER
                    | IMINSTRUCTION REGISTER REGISTER NUMBER
                    | LSIMINSTRUCTION REGISTER NUMBER
                    | LSINSTRUCTION REGISTER LP REGISTER RP
                    | LSINSTRUCTION REGISTER NUMBER LP REGISTER RP
                    | LSINSTRUCTION REGISTER STRING
                    '''
    p[0] = []
    for i in range(len(p)):
        if i != 0:
            p[0].append(p[i])


def p_error( p ):
    print("Syntax error in input:")
    print(p)
parser = yacc.yacc()

# read in file
f = open("input.asm", "r")
s = f.read()
s += 'EndOfFile: \n'

res = parser.parse(s)
f = open("parser.txt", "w")
for segment in curProgram.segment_list:
    f.write('index: {0}, type: {1}, label: {2}'.format(segment.index, segment.seg_type, segment.label))
    f.write("\n")
    f.write(str(segment.stmt_list))
    f.write("\n")
