from ply import lex

tokens = (
    'LP', 'RP', 'IMINSTRUCTION', 'RINSTRUCTION', 'LSINSTRUCTION', 'LSIMINSTRUCTION', 'JUMPINSTRUCTION',
    'BRANCHINSTRUCTION', 'SYSCALL', 'REGISTER', 'SEGMENT', 'DIRECTIVE', 'LABEL', 'COMMENT', 'STRING', 'NUMBER', 'EOL'
)

t_ignore = ' ,\t'
def t_LP(t):
    r'\('
    return t

def t_RP(t):
    r'\)'
    return t

def t_IMINSTRUCTION(t):
    r'(addi\b|subi\b|andi\b|ori\b|xori\b|beq\b|bneq\b)'
    return t

def t_RINSTRUCTION(t):
    r'(add\b|sub\b|mul\b|div\b|and\b|or\b|xor\b|nor\b|slt\b|sll\b|srl\b|sra\b)'
    return t

def t_LSINSTRUCTION(t):
    r'(lw\b|lb\b|sw\b|sb\b|la\b)'
    return t

def t_LSIMINSTRUCTION(t):
    r'li\b'
    return t

def t_JUMPINSTRUCTION(t):
    r'j\b'
    return t

def t_BRANCHINSTRUCTION(t):
    r'(beq\b|bne\b|bgtz\b|bltz\b|bgez\b|blez\b)'
    return t

def t_SYSCALL(t):
    r'syscall\b'
    return t

def t_REGISTER(t):
    r'\$(t|v|a)[0-9]'
    return t

def t_SEGMENT(t):
    r'(.text\b|.data\b|.rdata\b|.sdata\b)'
    return t

def t_DIRECTIVE(t):
    r'(.align\b|.ascii\b|.asciiz\b)'
    return t

def t_LABEL(t):
    r'[a-zA-Z0-9_]+:'
    return t

def t_COMMENT (t):
    r'\#.+'
    pass

def t_NUMBER( t ) :
    r'[0-9]+'
    t.value = int( t.value )
    return t

def t_EOL( t ):
  r'\n+'
  t.lexer.lineno += len( t.value )
  return t

t_STRING = r'[ a-zA-Z0-9~!@$%^&*()_+\{\}|:"<>?`\[\]\;\',]+'

def t_error( t ):
  print("Invalid Token:", t.value[0])
  t.lexer.skip( 1 )

lexer = lex.lex()


# f = open("input.asm", "r")
# s = f.read()
# lex.input(s)
# while True:
#     tok = lex.token()
#     if not tok: break
#     print(str(tok.type) + str(tok.value))