import re
import sys
# A lot of calling used, minimized by tokenize one sentence each time
float_types = {'half', 'bfloat', 'float', 'double', 'fp128', 'x86_fp80', 'ppc_fp128'}
param_attribs = {'zeroext', 'signext', 'inreg', 'byval', 'preallocated', 'inalloca', 'sret', 'align', 'noalias',
                 'nocapture', 'nofree', 'nest', 'returned', 'nonnull', 'dereferenceable', 'dereferenceable_or_null',
                 'swiftself', 'swifterror', 'immarg'}
terminator_ins = {'ret', 'br', 'switch', 'indirectbr', 'invoke', 'callbr', 'resume', 'catchswitch', 'catchret',
                  'cleanupret', 'unreachable'}
unary_opers = {'fneg'}
binary_opers = {'add', 'fadd', 'sub', 'fsub', 'mul', 'fmul', 'udiv', 'sdiv', 'fdiv', 'urem', 'srem', 'frem'}
bit_binary_opers = {'shl', 'lshr', 'ashr', 'and', 'or', 'xor'}
vect_opers = {'extractelement', 'insertelement', 'shuffleevector'}
aggre_opers = {'extractvalue', 'insertvalue'}
mem_opers = {'alloca', 'load', 'store', 'fence', 'cmpschg', 'atomicrmw', 'getelementptr', 'inbounds'}
conv_opers = {'trunc', 'zext', 'sext', 'fptrunc', 'fpext', 'fptoui', 'fptosi', 'uitofp', 'sitofp', 'ptrtoint',
              'inttoptr', 'bitcast', 'addrspacecast', 'to'}
other_opers = {'icmp', 'fcmp', 'phi', 'select', 'freeze', 'call', 'va_arg', 'landingpad', 'catchpad', 'cleanuppad'}
fence_args = {'acquire', 'release', 'acq_rel', 'seq_cst'}
icmp_args = {'eq', 'ne', 'ugt', 'uge', 'ult', 'ule', 'sgt', 'sge', 'slt', 'sle'}
fcmp_args = {'false', 'oeq', 'ogt', 'oge', 'olt', 'ole', 'one', 'ord', 'ueq', 'ugt', 'uge', 'ult', 'ule', 'une', 'uno',
             'true'}
function_kws = {'define', 'declare'}
array_multi = {'x'}
void_type = {'void'}
class token_template:
    def __init__(self, type_name, value):
        self.type_name = type_name
        self.value = value

def tokenize(read_in_file):
    line_num = 0
    tokens = []
    for line in read_in_file.splitlines():
        if len(line) == 0 or line == '\n' or line.startswith(';'):
            continue
        print(line)
        line_num += 1
        scan_line(line, tokens)
        # test
    for x in tokens:
        print("({}): {}".format(x.type_name, x.value), end=' || ')

def match_and_store(reg_str, comp_str, type_name, tokens, cur):
    token_val = re.search(reg_str, comp_str)
    if not token_val:
        # print('not match')
        return cur
    else:
        tokens.append(token_template(type_name, token_val.group()))
        cur += len(token_val.group())
    return cur

# def match_and_store_pointer(reg_str, comp_str, type_name, tokens, cur):
#     token_val = re.search(reg_str, comp_str)
#     if not token_val:
#         print("Error! match and store:")
#         print(comp_str)
#     else:
#         tokens.append(token_template(type_name, token_val.group()))
#         cur += len(token_val.group())
#     return cur

def token_iden(cur, line, tokens):
    if line[cur] == '@':
        if re.match('[-a-zA-Z$._]', line[cur + 1]) is not None:
            reg_str = '@[-a-zA-Z$._][-a-zA-Z$._0-9]*'
            type_name = 'global_named_iden'
            cur = match_and_store(reg_str, line[cur:], type_name, tokens, cur)
        else:
            reg_str = '@[0-9]*'
            type_name = 'global_unnamed_iden'
            cur = match_and_store(reg_str, line[cur:], type_name, tokens, cur)
    else:
        if re.match('[-a-zA-Z$._]', line[cur + 1]) is not None:
            reg_str = '%[-a-zA-Z$._][-a-zA-Z$._0-9]*'
            type_name = 'local_named_iden'
            cur = match_and_store(reg_str, line[cur:], type_name, tokens, cur)
        else:
            reg_str = '%[0-9]*'
            type_name = 'local_unnamed_iden'
            cur = match_and_store(reg_str, line[cur:], type_name, tokens, cur)
    return cur

def token_equal(cur, tokens):
    tokens.append(token_template('equal', '='))
    cur += 1
    return cur

def token_attr(cur, line, tokens):
    reg_str = '#[0-9]*'
    type_name = 'attributes'
    cur = match_and_store(reg_str, line[cur:], type_name, tokens, cur)
    return cur

def token_num(cur, line, tokens):
    cur = match_and_store('-?\d+\.?\d*(?:[eE][+-]?\d+)?', line[cur:], 'number', tokens, cur)
    return cur

def token_value_type(cur, line, tokens):
    temp = cur
    if line[cur] == 'i':
        if re.match('i[0-9]+\*', line[cur:]):
            cur = match_and_store('i[0-9]+\*', line[cur:], 'integer_type_pointer', tokens, cur)
        cur = match_and_store('i[0-9]+', line[cur:], 'integer_type', tokens, cur)
    else:
        if re.match('[a-z][a-z0-9_]*\*', line[cur:]):
            cur = match_and_store('[a-z][a-z0-9_]*\*', line[cur:], 'float_type_pointer', tokens, cur)
        if temp != cur:
            return cur
        token_val = re.search('[a-z][a-z0-9_]*', line[cur:])
        if token_val.group() in float_types:
            cur += len(token_val.group())
            tokens.append(token_template('float_type', token_val.group()))
        else:
            pass
    return cur

def token_array(cur, line, tokens):
    array_type_name = "left_array_curly" if line[cur] == '[' else "right_array_curly"
    tokens.append(token_template(array_type_name, line[cur]))
    cur += 1
    if line[cur] == '*':
        tokens.append(token_template('array_pointer', line[cur]))
        cur += 1
    return cur

def token_structure(cur, line, tokens):
    if line[cur] in ['{', '}']:
        array_type_name = "left_normal_structure" if line[cur] == '{' else "right_normal_structure"
        tokens.append(token_template(array_type_name, line[cur]))
        cur += 1
        return cur
    if line[cur] in ['<', '>']:
        array_type_name = "left_packed_structure" if line[cur] == '<' else "right_packed_structure"
        tokens.append(token_template(array_type_name, line[cur: cur + 2]))
        cur += 2
        return cur

def token_lists(cur, line, tokens):
    array_type_name = "left_list_paren" if line[cur] == '(' else "right_list_paren"
    tokens.append(token_template(array_type_name, line[cur]))
    cur += 1
    return cur

def token_terminator(cur, line, tokens):
    temp = cur + 1
    while line[temp] not in [' ', ','] and temp != len(line) - 1:
        temp += 1
    this_token = line[cur: temp]
    cur += len(this_token)
    if this_token in terminator_ins:
        tokens.append(token_template('terminator instruction', this_token))
    elif this_token in unary_opers:
        tokens.append(token_template('unary operation', this_token))
    elif this_token in binary_opers:
        tokens.append(token_template('binary operation', this_token))
    elif this_token in bit_binary_opers:
        tokens.append(token_template('bitwise binary operation', this_token))
    elif this_token in vect_opers:
        tokens.append(token_template('vector operation', this_token))
    elif this_token in aggre_opers:
        tokens.append(token_template('aggregate operation', this_token))
    elif this_token in mem_opers:
        tokens.append(token_template('memory operation', this_token))
    elif this_token in conv_opers:
        tokens.append(token_template('conversation operation', this_token))
    elif this_token in other_opers:
        tokens.append(token_template('other operation', this_token))
    # parameter attributes
    elif this_token in param_attribs:
        tokens.append(token_template('parameter attributes', this_token))
    # arguments
    elif this_token in fence_args:
        tokens.append(token_template('fence arguments', this_token))
    elif this_token in icmp_args:
        tokens.append(token_template('icmp arguments', this_token))
    elif this_token in fcmp_args:
        tokens.append(token_template('fcmp arguments', this_token))
    #function arguments
    elif this_token in function_kws:
        tokens.append((token_template('function keywords', this_token)))
    # array x
    elif this_token in array_multi:
        tokens.append((token_template('array multi', this_token)))
    # void type
    elif this_token in void_type:
        tokens.append((token_template('void_type', this_token)))
    else:
        cur -= len(this_token)
    return cur


def scan_line(line, tokens):
    cur = 0
    while cur < len(line):
        if line[cur].isspace():
            cur += 1
            continue
        if line[cur] == ';':
            break
        if line[cur] in ['%', '@']:
            cur = token_iden(cur, line, tokens)
            continue
        if line[cur] == '=':
            cur = token_equal(cur, tokens)
            continue
        if line[cur] == '#':
            cur = token_attr(cur, line, tokens)
            continue
        if re.fullmatch('-|[0-9]', line[cur]):
            cur = token_num(cur, line, tokens)
            continue
        if re.fullmatch('[a-z]', line[cur]):
            temp = cur
            cur = token_terminator(cur, line, tokens)
            if not temp == cur:
                continue
        if line[cur] in ['i', 'f', 'b', 'h', 'd', 'x', 'p']:
            cur = token_value_type(cur, line, tokens)
            continue
        if line[cur] == 'l' and line[cur: cur + 5] == 'label':
            cur += 5
            tokens.append(token_template('label_type', 'label'))
            continue
        if line[cur] == 't' and line[cur: cur + 5] == 'token':
            cur += 5
            tokens.append(token_template('token_type', 'token'))
            continue
        if line[cur] == 'm' and line[cur: cur + 8] == 'matadata':
            cur += 8
            tokens.append(token_template('matadata_type', 'matadata'))
            continue
        if line[cur] == '[' or line[cur] == ']':
            cur = token_array(cur, line, tokens)
            continue
        if line[cur] == '{' or line[cur] == '}' or line[cur] == '<' or line[cur] == '>':
            cur = token_structure(cur, line, tokens)
            continue
        if line[cur] == '(' or line[cur] == ')':
            cur = token_lists(cur, line, tokens)
            continue
        if line[cur] == ',':
            tokens.append(token_template('comma', ','))
            cur += 1
            continue
        else:
            # cur = token_err(cur, line, tokens)
            print('find error: ')
            print(line[cur])
            for x in tokens:
                print("({}): {}".format(x.type_name, x.value))
            sys.exit()

# def main():
#     line = "%7 = bitcast [5 x i32]* %5 to i8*"
#
#     tokens = []
#     scan_line(line, tokens)
#     for x in tokens:
#         print("({}): {}".format(x.type_name, x.value), end=' || ')
#
# if __name__ == "__main__":
#     main()
