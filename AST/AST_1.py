import sys
import tokenizer
def main():
    filename = str(sys.argv[1])
    read_in_file = read_ir(filename)
    tokenizer.tokenize(read_in_file)

            
def read_ir(filename):
    read_in_file = ""
    read_start = False
    with open(filename) as f:
        for line in f: 
            if read_start == False:
                if not line.startswith("define"):
                    # ignore global attributes for now
                    continue
                else:
                    read_start = True
            else:
                if line.startswith("attributes #0"):
                    break
                else:
                    read_in_file += line
    return read_in_file

if __name__ == "__main__":
    main()