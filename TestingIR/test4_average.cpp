#include <iostream>

using namespace std;

// Calculate average of array

int main() {
    float arr[] = {5,6,10,11,12};
    int n;
    float sum = 0;
    float ave;

    n = sizeof(arr)/sizeof(arr[0]);
    for (int i=0; i < n; i++) {
        sum += arr[i];
    }
    ave = sum/n;
    std::cout << "Average of the array: "<< ave << "\n";
    
    return 0;
}