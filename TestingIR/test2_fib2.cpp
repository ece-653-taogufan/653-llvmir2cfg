#include <iostream>

using namespace std;

// Fibonacci Series using DP

int main() {
    int n;
    std::cout << "Input an integer for the order in Fibonacci Series: \n";
    std::cin >> n;
    int f[n+2];
    f[0] = 0;
    f[1] = 1;
    for (int i=2; i <= n; i++){
        f[i] = f[i-1] + f[i-2];
    }
    std::cout <<"fib(" << n << ") = " << f[n] <<"\n";
    return f[n];
}