#include <iostream>

using namespace std;

// Determine if Point D is inside triangle (A,B,C)

struct Point{
    float x,y;
};

// pass struct by reference
float get_area(Point &A, Point &B, Point &C){
    float res;
    res = (A.x*(B.y-C.y)+B.x*(C.y-A.y)+C.x*(A.y-B.y))/2.0;
    if (res >=0){
        return res;
    }
    else {
        return -res;
    }
}

bool is_inside(Point &A, Point &B, Point &C, Point &D){
    float total_area, area1, area2, area3;
    total_area = get_area(A,B,C);
    area1 = get_area(A,B,D);
    area2 = get_area(A,C,D);
    area3 = get_area(B,C,D);
    return (total_area == area1 + area2 + area3);
}

int main() {
    Point A, B, C, D;
    bool inside;

    A.x = 0;
    A.y = 0;
    B.x = 3;
    B.y = 5;
    C.x = 8;
    C.y = 0;
    D.x = 3;
    D.y = 12;

    inside = is_inside(A,B,C,D);
    if (inside) {
        std::cout <<"D is inside triangle(A,B,C)" << "\n";
    }
    else {
        std::cout <<"D is not inside triangle(A,B,C)" << "\n";
    }
    return 0;
}