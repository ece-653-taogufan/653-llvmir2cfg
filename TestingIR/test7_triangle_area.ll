; ModuleID = 'test7_triangle_area.cpp'
source_filename = "test7_triangle_area.cpp"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%"class.std::ios_base::Init" = type { i8 }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_get"* }
%"class.std::ios_base" = type { i32 (...)**, i64, i64, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type { i8*, i64 }
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i64, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type <{ i32 (...)**, i32, [4 x i8] }>
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::ctype" = type <{ %"class.std::locale::facet.base", [4 x i8], %struct.__locale_struct*, i8, [7 x i8], i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8, [6 x i8] }>
%"class.std::locale::facet.base" = type <{ i32 (...)**, i32 }>
%struct.__locale_struct = type { [13 x %struct.__locale_data*], i16*, i32*, i32*, [13 x i8*] }
%struct.__locale_data = type opaque
%"class.std::num_put" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::num_get" = type { %"class.std::locale::facet.base", [4 x i8] }
%struct.Point = type { float, float }

@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@__dso_handle = external hidden global i8
@_ZSt4cout = external dso_local global %"class.std::basic_ostream", align 8
@.str = private unnamed_addr constant [26 x i8] c"Area of this triangle is \00", align 1
@.str.1 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_test7_triangle_area.cpp, i8* null }]

; Function Attrs: noinline uwtable
define internal void @__cxx_global_var_init() #0 section ".text.startup" {
  call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  %1 = call i32 @__cxa_atexit(void (i8*)* bitcast (void (%"class.std::ios_base::Init"*)* @_ZNSt8ios_base4InitD1Ev to void (i8*)*), i8* getelementptr inbounds (%"class.std::ios_base::Init", %"class.std::ios_base::Init"* @_ZStL8__ioinit, i32 0, i32 0), i8* @__dso_handle) #3
  ret void
}

declare dso_local void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"*) unnamed_addr #1

; Function Attrs: nounwind
declare dso_local void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"*) unnamed_addr #2

; Function Attrs: nounwind
declare dso_local i32 @__cxa_atexit(void (i8*)*, i8*, i8*) #3

; Function Attrs: noinline nounwind optnone uwtable
define dso_local float @_Z8get_areaR5PointS0_S0_(%struct.Point* dereferenceable(8) %0, %struct.Point* dereferenceable(8) %1, %struct.Point* dereferenceable(8) %2) #4 {
  %4 = alloca float, align 4
  %5 = alloca %struct.Point*, align 8
  %6 = alloca %struct.Point*, align 8
  %7 = alloca %struct.Point*, align 8
  %8 = alloca float, align 4
  store %struct.Point* %0, %struct.Point** %5, align 8
  store %struct.Point* %1, %struct.Point** %6, align 8
  store %struct.Point* %2, %struct.Point** %7, align 8
  %9 = load %struct.Point*, %struct.Point** %5, align 8
  %10 = getelementptr inbounds %struct.Point, %struct.Point* %9, i32 0, i32 0
  %11 = load float, float* %10, align 4
  %12 = load %struct.Point*, %struct.Point** %6, align 8
  %13 = getelementptr inbounds %struct.Point, %struct.Point* %12, i32 0, i32 1
  %14 = load float, float* %13, align 4
  %15 = load %struct.Point*, %struct.Point** %7, align 8
  %16 = getelementptr inbounds %struct.Point, %struct.Point* %15, i32 0, i32 1
  %17 = load float, float* %16, align 4
  %18 = fsub float %14, %17
  %19 = fmul float %11, %18
  %20 = load %struct.Point*, %struct.Point** %6, align 8
  %21 = getelementptr inbounds %struct.Point, %struct.Point* %20, i32 0, i32 0
  %22 = load float, float* %21, align 4
  %23 = load %struct.Point*, %struct.Point** %7, align 8
  %24 = getelementptr inbounds %struct.Point, %struct.Point* %23, i32 0, i32 1
  %25 = load float, float* %24, align 4
  %26 = load %struct.Point*, %struct.Point** %5, align 8
  %27 = getelementptr inbounds %struct.Point, %struct.Point* %26, i32 0, i32 1
  %28 = load float, float* %27, align 4
  %29 = fsub float %25, %28
  %30 = fmul float %22, %29
  %31 = fadd float %19, %30
  %32 = load %struct.Point*, %struct.Point** %7, align 8
  %33 = getelementptr inbounds %struct.Point, %struct.Point* %32, i32 0, i32 0
  %34 = load float, float* %33, align 4
  %35 = load %struct.Point*, %struct.Point** %5, align 8
  %36 = getelementptr inbounds %struct.Point, %struct.Point* %35, i32 0, i32 1
  %37 = load float, float* %36, align 4
  %38 = load %struct.Point*, %struct.Point** %6, align 8
  %39 = getelementptr inbounds %struct.Point, %struct.Point* %38, i32 0, i32 1
  %40 = load float, float* %39, align 4
  %41 = fsub float %37, %40
  %42 = fmul float %34, %41
  %43 = fadd float %31, %42
  %44 = fpext float %43 to double
  %45 = fdiv double %44, 2.000000e+00
  %46 = fptrunc double %45 to float
  store float %46, float* %8, align 4
  %47 = load float, float* %8, align 4
  %48 = fcmp oge float %47, 0.000000e+00
  br i1 %48, label %49, label %51

49:                                               ; preds = %3
  %50 = load float, float* %8, align 4
  store float %50, float* %4, align 4
  br label %54

51:                                               ; preds = %3
  %52 = load float, float* %8, align 4
  %53 = fneg float %52
  store float %53, float* %4, align 4
  br label %54

54:                                               ; preds = %51, %49
  %55 = load float, float* %4, align 4
  ret float %55
}

; Function Attrs: noinline norecurse optnone uwtable
define dso_local i32 @main() #5 {
  %1 = alloca i32, align 4
  %2 = alloca %struct.Point, align 4
  %3 = alloca %struct.Point, align 4
  %4 = alloca %struct.Point, align 4
  %5 = alloca float, align 4
  store i32 0, i32* %1, align 4
  %6 = getelementptr inbounds %struct.Point, %struct.Point* %2, i32 0, i32 0
  store float 0.000000e+00, float* %6, align 4
  %7 = getelementptr inbounds %struct.Point, %struct.Point* %2, i32 0, i32 1
  store float 0.000000e+00, float* %7, align 4
  %8 = getelementptr inbounds %struct.Point, %struct.Point* %3, i32 0, i32 0
  store float 3.000000e+00, float* %8, align 4
  %9 = getelementptr inbounds %struct.Point, %struct.Point* %3, i32 0, i32 1
  store float 5.000000e+00, float* %9, align 4
  %10 = getelementptr inbounds %struct.Point, %struct.Point* %4, i32 0, i32 0
  store float 8.000000e+00, float* %10, align 4
  %11 = getelementptr inbounds %struct.Point, %struct.Point* %4, i32 0, i32 1
  store float 0.000000e+00, float* %11, align 4
  %12 = call float @_Z8get_areaR5PointS0_S0_(%struct.Point* dereferenceable(8) %2, %struct.Point* dereferenceable(8) %3, %struct.Point* dereferenceable(8) %4)
  store float %12, float* %5, align 4
  %13 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str, i64 0, i64 0))
  %14 = load float, float* %5, align 4
  %15 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEf(%"class.std::basic_ostream"* %13, float %14)
  %16 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %15, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.1, i64 0, i64 0))
  ret i32 0
}

declare dso_local dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272), i8*) #1

declare dso_local dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEf(%"class.std::basic_ostream"*, float) #1

; Function Attrs: noinline uwtable
define internal void @_GLOBAL__sub_I_test7_triangle_area.cpp() #0 section ".text.startup" {
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }
attributes #4 = { noinline nounwind optnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noinline norecurse optnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"Ubuntu clang version 10.0.1-++20200626113502+77d76b71d7d-1~exp1~20200626214123.189 "}
