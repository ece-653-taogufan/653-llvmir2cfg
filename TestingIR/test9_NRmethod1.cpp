#include <iostream>

using namespace std;

// Find root for function x^3 + 2*x^2 -5 using Newton Raphson Method

float find_root(float x){
    float delta = 1;
    float eps = 1e-4;
    while (abs(delta) >= eps) {
        delta = (x*x*x + 2*x*x -5)/(3*x*x+4*x);
        x -= delta;

    }
    return x;
}

int main() {
    float x = 2;
    float root;
    
    root = find_root(x);
    std::cout << "Root for x^3 + 2*x^2 -5 is " << root << "\n";
    
    return 0;
}