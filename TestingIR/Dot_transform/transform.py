# transform LLVM IR to dot file
import re

# get function name and delete irrelevant parts
def read_function(content):
    function = []
    flag = 0
    for line in content:
        if "define" in line:
            flag = 1
            pattern = "\@(.*?)\("
            func_name = re.search(pattern, line).group(1)
        if "attributes" in line: flag = 0
        if flag:
            if line: function.append(line)

    return func_name, function


# get the content of each block
def read_block(function):
    block = ['1']
    num = [1]
    bb_dict = {}
    for line in function:
        pattern = "(\d+)\:"
        bb_num = re.search(pattern, line)
        if bb_num != None:
            split = function.index(line)
            num.append(split)
            block.append(bb_num.group(1))

    for i in range(len(block)):
        if i == len(block)-1:
            bb_dict[block[i]] = function[num[i]:]
        else:
            bb_dict[block[i]] = function[num[i]:num[i+1]]

    return bb_dict


# get the control flow of blocks
def read_flow(bb_dict):
     flow_dict = {}
     for key in bb_dict:
         for line in bb_dict[key]:
             pattern = "label \%(\d+)"
             next_key = re.search(pattern, line)
             if next_key != None:
                 keys = re.findall(pattern, line)
                #  print(keys)
                 
                 flow_dict[key] = keys                

     return flow_dict
    


def transform(test_file):
    with open(test_file) as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    # print(content)
    name, function = read_function(content)
    # print(function)
    # print(name)
    bb_dict = read_block(function)
    print(bb_dict)
    # print(read_flow(bb_dict))
    flow_dict = read_flow(bb_dict)
    # print(flow_dict)
    # G = pgv.AGraph()
    # G.add_node('a')
    # G.add_edge(content)
    with open ('./graph.dot','w') as out:
        for line in ('digraph \"CFG for '+ name + ' function\" {', 'size = "16,16";','label = \"CFG for '+ name + ' function\";'):
            out.write('{}\n'.format(line))
        for key in bb_dict: 
            label = '\l'.join(bb_dict[key])
            out.write('{} [shape=record,label="{}"];\n'.format(key, label))
        for key in flow_dict:
            for next_key in flow_dict[key]:
                out.write('{} -> {};\n'.format(key, next_key))
        out.write('}\n')
            

transform('./test.ll')
