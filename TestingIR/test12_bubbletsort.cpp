#include <iostream>

using namespace std;

// Find root for function x^5 + x^3 -20 using Newton Raphson Method with function calls

void swap(float *x, float *y) {
    float temp;
    temp = *y;
    *y = *x;
    *x = temp;
}

void bubble_sort(float arr[], int n) {
    bool swapped = true;
    int last_swap;
    while (swapped) {
        swapped = false;
        for (int i=1; i < n; i++){
            if (arr[i-1] > arr[i]) {
                swap(&arr[i-1], &arr[i]);
                last_swap = i;
                swapped = true;
            }
        }
        n = last_swap;
    }
}

int main() {
    float arr[] = {3.2,5.3,2.1,-0.5,-9.6,5.5,6.7,6.12,9.26,2.1,3,-8.2,-0.5};
    int n;

    n = sizeof(arr)/sizeof(arr[0]);
    bubble_sort(arr, n);
    for (int i=0; i<n; i++) {
        std::cout << arr[i] << "  ";
    }
    std::cout << "\n";
    return 0;
}