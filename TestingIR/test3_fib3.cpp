#include <iostream>

using namespace std;

// Fibonacci Series using DP with space optimization

int main() {
    int n;
    std::cout << "Input an integer for the order in Fibonacci Series: \n";
    std::cin >> n;
    int f1, f2, f3;
    f1 = 0;
    f2 = 1;
    for (int i=2; i <= n; i++){
        f3 = f1 + f2;
        f1 = f2;
        f2 = f3;
    }
    if (n == 0){
        f3 = 0;
    }
    else if (n == 1){
        f3 = 1;
    }
    std::cout <<"fib(" << n << ") = " << f3 <<"\n";
    return f3;
}