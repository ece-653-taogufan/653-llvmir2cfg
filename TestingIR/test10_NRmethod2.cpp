#include <iostream>

using namespace std;

// Find root for function x^5 + x^3 -20 using Newton Raphson Method with function calls

float func(float x) {
    return x*x*x*x*x + x*x*x - 20;
}

float dev_func(float x) {
    return 5*x*x*x*x + 3*x*x;
}

float find_root(float x){
    float delta = 1;
    float eps = 1e-5;
    while (abs(delta) >= eps) {
        delta = func(x)/dev_func(x);
        x -= delta;
    }
    return x;
}

int main() {
    float x = 3;
    float root;
    
    root = find_root(x);
    std::cout << "Root for x^5 + x^3 -20 is " << root << "\n";
    
    return 0;
}