#include <iostream>

using namespace std;

// Fibonacci Series using recursions

int fib(int n){
    if (n <= 1){
        return n;
    }
    else {
        return fib(n-1) + fib(n-2);
    }
}

int main() {
    int n;
    std::cout << "Input an integer for the order in Fibonacci Series: \n";
    std::cin >> n;
    std::cout <<"fib(" << n << ") = " << fib(n) <<"\n";
    return 0;
}