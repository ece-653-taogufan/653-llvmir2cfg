#include <iostream>
#include <vector>

using namespace std;

// Calculate the running average of an array

int main() {
    float arr[] = {5,10,15,15,9.6,0.2};
    int n;
    float sum = 0;
    float run_ave;
    std::vector<float> ave;

    n = sizeof(arr)/sizeof(arr[0]);
    for (int i=0; i < n; i++) {
        sum += arr[i];
        run_ave = sum/(i+1);
        ave.push_back(run_ave);
    }
    for (int i=0; i < ave.size(); i++) {
        std::cout << "Average of the first "<< (i+1) << " numbers is " << ave[i] << "\n";
    }
    return 0;
}