#include <iostream>

using namespace std;

// Quick sort for an array of float

void swap(float *x, float *y) {
    float temp;
    temp = *y;
    *y = *x;
    *x = temp;
}

int partition(float arr[], int low, int high) {
    float pivot = arr[high];
    int p = low - 1;
    for (int i=low; i<high; i++) {
        if (arr[i] <= pivot) {
            p ++;
            swap(&arr[p], &arr[i]);
        }
    }
    swap(&arr[p+1],&arr[high]);
    return p+1;
}

void quick_sort(float arr[], int low, int high) {
    int p;
    if (low < high) {
        p = partition(arr, low, high);
        quick_sort(arr,low, p-1);
        quick_sort(arr, p+1, high);
    }
}

int main() {
    float arr[] = {3.2,5.5,1.0,0.5,-9.6,5.5,6.7,6.12,9.26,2.1,3,-8.2,-0.5};
    int n;

    n = sizeof(arr)/sizeof(arr[0]);
    quick_sort(arr, 0, n-1);
    for (int i=0; i<n; i++) {
        std::cout << arr[i] << "  ";
    }
    std::cout << "\n";
    return 0;
}