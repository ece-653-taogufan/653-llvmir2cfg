#include <iostream>
#include <iterator>

using namespace std;

// Calculate average of array using pointer

int main() {
    float arr[] = {3,9,15,9.5,23.3};
    int n = 0;
    float sum = 0;
    float ave;

    for (auto it=std::begin(arr); it != std::end(arr); it++) {
        sum += *it;
        n += 1;
    }
    ave = sum/n;
    std::cout << "Average of the array: "<< ave << "\n";
    
    return 0;
}