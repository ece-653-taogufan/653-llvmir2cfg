#include <iostream>

using namespace std;

// Calculate the triangle area given coordinates

struct Point{
    float x,y;
};

// pass struct by reference
float get_area(Point &A, Point &B, Point &C){
    float res;
    res = (A.x*(B.y-C.y)+B.x*(C.y-A.y)+C.x*(A.y-B.y))/2.0;
    if (res >=0){
        return res;
    }
    else {
        return -res;
    }
}

int main() {
    Point A, B, C;
    float area;

    A.x = 0;
    A.y = 0;
    B.x = 3;
    B.y = 5;
    C.x = 8;
    C.y = 0;

    area = get_area(A,B,C);
    std::cout << "Area of this triangle is " << area << "\n";
    return 0;
}