Update:
New goal: parsing target has changed to mips for smaller number of instructions and simpler structure


use python - ply as fex & yacc tool for cross-platform


--------------------------------------------------------------------------
# 653-LLVMIR2CFG

DUMP---
653 project, LLVMIR -> AST tree -> CFG

sample commands:


*  clang -S -emit-llvm test1.c -o test1.ll
*  opt -dot-cfg test1.ll
*  dot -Tpng -o test3.png dom.main.dot
*  To show .dot file: copy file to this website:  https://dreampuf.github.io/GraphvizOnline/

DUMP---